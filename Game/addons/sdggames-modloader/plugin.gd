tool
extends EditorPlugin


const PluginName = 'ModLoader'
const PluginPath = 'res://addons/sdggames-modloader/ModLoader.tscn'


func _enter_tree():
	self.add_autoload_singleton(PluginName, PluginPath)


func _exit_tree():
	self.remove_autoload_singleton(PluginName)
