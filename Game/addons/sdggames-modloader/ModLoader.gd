extends Node2D

export(String) var mods_path = "res://Mods"
export(Resource) var mod_select_scene

# Emitted when the mods are reloaded from disk. Any mod menu should be rebuilt if this happens.
signal mod_list_rebuilt

# Emitted when mods are enabled or disabled according to their mod states.
signal mods_reloaded

var mods_list: Array

func _ready():
	if OS.get_name() != "HTML5":
		mods_path = ProjectSettings.globalize_path(mods_path)
		
	rebuild_mod_list()
	reload_mods()


# Reloads all mods from the disk.
func rebuild_mod_list():
	for mod in mods_list:
		mod.queue_free()
	
	# Get an array of all mod folders
	var mods = _get_directory_list(mods_path)
		
	for dir in mods:
		var mod_select = _mod_list_from_directory(mods_path + "/" + dir)
		mods_list.append(mod_select)
	emit_signal("mod_list_rebuilt")


# Returns the top level names of every mod in the mods folder.
func get_mod_names():
	var names = []
	for mod in mods_list:
		names.append(mod.name)
	return names


# Loads all mods that are set to be enabled, and instantiates any singletons.
func reload_mods():
	for mod in mods_list:
		if mod.mod_type == Mod.ModType.AUTO_LOAD:
			mod.free_mod_singleton()

	yield(get_tree(), "idle_frame")
	
	for mod in mods_list:
		mod.load_mod()
	
	# Instantiate any singletons after all mods are loaded in case any
	# resources need to be overwritten first.
	for mod in mods_list:
		if mod.mod_type == Mod.ModType.AUTO_LOAD:
			mod.activate_mod_singleton()
	emit_signal("mods_reloaded")


# Checks if the mod exists.
func mod_exists(mod_name: String):
	# Look through my mods and return true if one matches the name.
	for mod in mods_list:
		if mod.name == mod_name:
			return true
	return false


# Returns the type of the mod.
func get_mod_type(mod_name: String):
	var mod = _get_mod(mod_name)
	return mod.mod_type


# Activates or deactivates the mod. Will only take effect after a restart.
func set_mod_active(mod_name: String, active: bool):
	var mod = _get_mod(mod_name)
	mod.is_active = active


# Returns the number of variants that the mod has.
func get_variant_count(mod_name: String):
	return _get_mod(mod_name).mod_variants.size()


# Returns an array containing all of the mod variant names.
func get_variant_names(mod_name: String):
	return _get_mod(mod_name).get_variant_names()


# Enables one mod variant and disables all others. ONLY WORKS FOR AUTOLOAD AND SELECT_ONE MODES!
func select_active_mod_variant(mod_name: String, index: int):
	var mod = _get_mod(mod_name)
	mod.select_active_mod_variant(index)


# Enables or disables the mod variant at index, ONLY WORKS FOR SELECT_ANY MODE!
func set_mod_variant_state(mod_name: String, index: int, active: bool):
	var mod = _get_mod(mod_name)
	mod.set_mod_variant_state(index, active)


# Helper function to get the mod that matches mod_name
func _get_mod(mod_name: String) -> Mod:
	# Look through my mods and return the one that matches the name.
	for mod in mods_list:
		if mod.name == mod_name:
			return mod

	# Error if we can't find a mod with that name.
	push_error(mod_name + " does not match any existing mod! Call mod_exists " \
			+ "first to check if the mod exists before trying to access it!")
	return null


# Helper function to load all subdirectories in directory.
func _get_directory_list(dirPath: String):
	var subDirList = []
	var directory = Directory.new()
	directory.open(dirPath)
	directory.list_dir_begin()
	var dir = directory.get_next()
	
	# Search through all files and folders to get only the directories.
	while dir != "":
		if directory.current_is_dir() && !dir.begins_with("."):
			subDirList.append(dir)
		dir = directory.get_next()
	
	return subDirList


# Helper function loads a mod list for each directory and sets the appropriate flags.
func _mod_list_from_directory(dirPath: String):
	# Create a list of variants.
	var variantList: Mod = mod_select_scene.instance()
	add_child(variantList)
	variantList.name = dirPath.split("/")[-1]
	
	# Open a new directory and start scannin the files.
	var directory = Directory.new()
	directory.open(dirPath)
	directory.list_dir_begin()
	var mod = directory.get_next()
	
	# Search through all files and folders to get only the files.
	while mod != "":
		if !directory.current_is_dir() && !mod.begins_with("."):
			# Use the flag files to set the variant list type.
			if mod == "ModType_Autoload":
				variantList.mod_type = variantList.ModType.AUTO_LOAD
			elif mod == "ModType_SelectOne":
				variantList.mod_type = variantList.ModType.SELECT_ONE
			elif mod == "ModType_SelectAny":
				variantList.mod_type = variantList.ModType.SELECT_ANY
			# If this is not a flag file, then add it to the variant list.
			else:
				variantList.add_mod(mod, dirPath + "/" + mod)
		mod = directory.get_next()

	variantList.initialize_mod()
	return variantList
