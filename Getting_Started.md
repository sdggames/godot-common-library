# Using this library as a starting template

This library contains multiple packages, tools, and extensions that I prefer to use in all of my games, even quick game jam entries. I may look into exporting this template at some point in the future, but right now, the easiest way to get up and running is to clone the repository, change "origin," then push to a new repo. 

This document is a step-by-step guide to doing just that.

## 1. Download the repository, enable LFS, and check out all subrepos.
Run these commands to clone the repository to your local machine. If you have already done this, double-check that you have properly pulled any large files (`git LFS`), and have pulled all submodules.

    git lfs install
    git clone https://gitlab.com/sdggames/godot-common-library.git GAME_FOLDER
    cd GAME_FOLDER
    git submodule update --init
    git lfs fetch --all

You may want to open the project in Godot at this point. If it reports missing or corrupted files, try again. If everything looks fine, close Godot and move on to the next step.

## 2. Create a new repo and change "origin" to point to it.
Create a GitLab repository for your game. You can use something else (GitHub, Bitbucket, etc.) if you prefer, but the CICD template will only work for Gitlab.
* Select "Create blank project" from [https://gitlab.com/projects/new](https://gitlab.com/projects/new)
* Run the following commands in your local repo:

        git remote rename origin old-origin
        git remote add origin YOUR_REPO
        git push -u origin --all
        git push -u origin --tags

* Check the CICD tab. The project should build automatically. You can play the web version by clicking "browse artifacts" on the `pack:WindowsPortable` job

## 3. Update the game name and release info
Check out VersionInfo.yml and adjust the following:

GROUP_NAME: Your studio title. Must match Itch.io for deployment*

GAME_NAME: The name of your game. Must match Itch.io for deployment*

RELEASE_NUMBER: Reset to 0.0.0 if using semver, or change to whatever you like.

GODOT_VERSION: Change to match your current version. Must be a released version, this is used to pull a Docker image for automated builds.

*This project can automatically deploy from the `master` branch to `GROUP_NAME`.itch.io/`GAME_NAME`

## 4. (Optional) Squash all commits
Squash all commits into one for a clean start to the repo. Note: it may be neccessary to temporarily enable force push on the Master Branch (`settings/repository`)

    git reset --soft 6a26dc2ca80cc6164f357ad561be760c6a2730d1
    git commit --amend -m "Base game template"
    git push -f

Once you have verified that the new repo contains everything that you need, you can delete the reference to the base library, if you want:

    git remote rm old-origin

## 5. Set up Gitlab settings and connect to Itch

* Create a project on Itch.io that matches the game URL as defined in VersionInfo.yml.

* In the Ci/Cd section, define a variable for `BUTLER_API_KEY`. If you don't have an API key, you can generate one from the developer console on Itch. One key can work for multiple projects.

* Run the `itchio-unstable` job in the pipeline on the master branch