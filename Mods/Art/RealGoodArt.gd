extends Control

var textures: Array
var lastArt = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	# Create a list of variants.
	var directory = Directory.new()
	directory.open("res://Art")
	directory.list_dir_begin()
	var art = directory.get_next()

	# Get a list of all png files.
	while art != "":
		# Weird behaviour, but import files exist while .png files don't. ¯\_(ツ)_/¯
		if !directory.current_is_dir() && art.ends_with(".png.import"):
			textures.append(load("res://Art/" + art.replace(".import", "")))
		art = directory.get_next()


func _on_NewArt_pressed():
	if textures.size() > 1:
		var index = lastArt
		while index == lastArt:
			index = randi() % textures.size()
		$"CenterContainer/TextureRect".texture = textures[index]
		lastArt = index
