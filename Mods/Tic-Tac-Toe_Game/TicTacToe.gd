extends CenterContainer

export(Texture) var empty_cell_texture
export(Texture) var x_select_texture
export(Texture) var o_select_texture

enum {X, O, NONE}

var game_grid
var game_states = [ [ NONE, NONE, NONE, ], [ NONE, NONE, NONE, ], [ NONE, NONE, NONE, ], ]
var active_player = X

func _ready():
	game_grid = [ [ $"Board/1_1", $"Board/1_2", $"Board/1_3" ],
			[ $"Board/2_1", $"Board/2_2", $"Board/2_3" ],
			[ $"Board/3_1", $"Board/3_2", $"Board/3_3" ], ]
	
	reset_game()


func reset_game():
	game_states = [ [ NONE, NONE, NONE, ], [ NONE, NONE, NONE, ], [ NONE, NONE, NONE, ], ]
	for x in game_grid:
		for s in x:
			reset_cell(s)
	set_active_team(X)


func change_team():
	if active_player == X:
		active_player = O
	else:
		active_player = X
	
	set_active_team(active_player)


func set_active_team(team):
	active_player = team
	var player_texture = get_team_texture(team)
	
	for x in game_grid:
		for cell in x:
			if !cell.disabled:
				cell.texture_pressed = player_texture
				cell.texture_hover = player_texture


func get_team_texture(team):
	var player_texture: Texture
	if team == X:
		player_texture = x_select_texture
	elif team == O:
		player_texture = o_select_texture
	return player_texture


func reset_cell(cell: TextureButton):
	unlock_cell(cell)
	cell.texture_normal = empty_cell_texture


func unlock_cell(cell: TextureButton):
	cell.disabled = false
	cell.pressed = false


func lock_cell(cell: TextureButton):
	cell.disabled = true
	cell.texture_disabled = cell.texture_pressed
	cell.texture_normal = cell.texture_pressed


func checkWinCondition():
	var winner = NONE
	var tie = false
	if ( (game_states[0][0] == X && game_states[0][1] == X && game_states[0][2] == X) ||    #H1
			(game_states[1][0] == X && game_states[1][1] == X && game_states[1][2] == X) || #H2
			(game_states[2][0] == X && game_states[2][1] == X && game_states[2][2] == X) || #H3
			(game_states[0][0] == X && game_states[1][0] == X && game_states[2][0] == X) || #V1
			(game_states[0][1] == X && game_states[1][1] == X && game_states[2][1] == X) || #V2
			(game_states[0][2] == X && game_states[1][2] == X && game_states[2][2] == X) || #V3
			(game_states[0][0] == X && game_states[1][1] == X && game_states[2][2] == X) || #D1
			(game_states[0][2] == X && game_states[1][1] == X && game_states[2][0] == X) ): #D2
		winner = X
	
	elif ( (game_states[0][0] == O && game_states[0][1] == O && game_states[0][2] == O) ||    #H1
			(game_states[1][0] == O && game_states[1][1] == O && game_states[1][2] == O) || #H2
			(game_states[2][0] == O && game_states[2][1] == O && game_states[2][2] == O) || #H3
			(game_states[0][0] == O && game_states[1][0] == O && game_states[2][0] == O) || #V1
			(game_states[0][1] == O && game_states[1][1] == O && game_states[2][1] == O) || #V2
			(game_states[0][2] == O && game_states[1][2] == O && game_states[2][2] == O) || #V3
			(game_states[0][0] == O && game_states[1][1] == O && game_states[2][2] == O) || #D1
			(game_states[0][2] == O && game_states[1][1] == O && game_states[2][0] == O) ): #D2
		winner = O
	
	elif ( game_states[0][0] != NONE && game_states[0][1] != NONE && game_states[0][2] != NONE &&
			game_states[1][0] != NONE && game_states[1][1] != NONE && game_states[1][2] != NONE &&
			game_states[2][0] != NONE && game_states[2][1] != NONE && game_states[2][2] != NONE ):
		tie = true

	if tie:
		active_player = NONE
		for x in game_grid:
			for cell in x:
				unlock_cell(cell)
	else:
		match winner:
			NONE:
				change_team()
			X, O:
				active_player = NONE
				for x in game_grid:
					for cell in x:
						reset_cell(cell)
						cell.texture_normal = get_team_texture(winner)
						cell.texture_pressed = get_team_texture(winner)
						cell.texture_hover = get_team_texture(winner)


func _on_cell_pressed(x: int, y: int):
	match active_player:
		NONE:
			reset_game()
		X, O:
			game_states[x][y] = active_player
			
			lock_cell(game_grid[x][y])
			
			checkWinCondition()
